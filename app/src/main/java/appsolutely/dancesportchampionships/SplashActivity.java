package appsolutely.dancesportchampionships;

import  android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;

import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.fragments.AdjudicatorsFragment;
import appsolutely.dancesportchampionships.fragments.AudienceFragment;
import appsolutely.dancesportchampionships.fragments.CompetitorsFragment;
import appsolutely.dancesportchampionships.utilities.Constant;
import appsolutely.dancesportchampionships.utilities.DataCache;

public class SplashActivity extends Activity implements OnClickListener{
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView imgViewLogo = (ImageView)findViewById(R.id.imgViewLogo);

        Picasso.get()
                .load(Constant.Urls.EVENT_LOGO)
                .into(imgViewLogo);
        findViewById(R.id.home_link_competitors).setOnClickListener(this);
        findViewById(R.id.home_link_audience).setOnClickListener(this);
        findViewById(R.id.home_link_adjudicators).setOnClickListener(this);
        getActionBar().hide();
    }

    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        switch (v.getId()) {
            case R.id.home_link_audience:
                intent.putExtra("fragment", HomeFragment.getName(AudienceFragment.fragmentId));
                break;
            case R.id.home_link_adjudicators:
                intent.putExtra("fragment", HomeFragment.getName(AdjudicatorsFragment.fragmentId));
                break;
            case R.id.home_link_competitors:
                intent.putExtra("fragment", HomeFragment.getName(CompetitorsFragment.fragmentId));
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }


    }

