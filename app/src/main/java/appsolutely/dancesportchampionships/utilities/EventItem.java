package appsolutely.dancesportchampionships.utilities;

public class EventItem {
    public String name;
    public String school;
    public String tag;

    public EventItem(String tag, String name, String school) {
        this.tag = tag;
        this.name = name;
        this.school = school;
    }
}
