package appsolutely.dancesportchampionships.utilities;

import java.util.ArrayList;
import java.util.List;

public class Participant {
    public String country;
    public List<String> events;
    public String group;
    public String name;
    public String paid;
    public String pin;
    public String pointOfContact;
    public String room;
    public String tag;
    public int total;

    public static Participant m8n(String[] details) {
        Participant participant = new Participant();
        participant.events = new ArrayList();
        participant.pointOfContact = details[0].trim();
        participant.tag = details[1].trim();
        participant.name = details[2].trim();
        participant.country = details[3].trim();
        participant.events.add(details[4].trim());
        participant.room = details[5].trim();
        participant.paid = details[6].trim();
        participant.group = details[7].trim();
        participant.pin = details[8].trim();
        participant.total = getNumber(details[9].trim());
        return participant;
    }

    private static int getNumber(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public String[] toArguments() {
        return new String[]{tag, name, country, getEventsString(), room, paid, String.valueOf(total)};
    }

    public String toString() {
        return this.tag + ". " + this.name;
    }

    public boolean isProfessional() {
        for (String event : this.events) {
            if (event.contains("Professional")) {
                return true;
            }
        }
        return false;
    }

    public String getEventsString() {
        String output = "";
        for (String event : this.events) {
            output = new StringBuilder(String.valueOf(output)).append("\n").append(event).toString();
        }
        return output;
    }
}
