package appsolutely.dancesportchampionships.utilities;

public class Judge {
    public String alpha;
    public String country;
    public String desc;
    public String name;
    public String pdfUrl;
    public String pin;
    public int position;

    public static Judge m7n(int position, String image, String name, String country, String desc, String alpha, String pin) {
        Judge judge = new Judge();
        judge.position = position;
        judge.pdfUrl = image;
        judge.name = name;
        judge.country = country;
        judge.desc = desc;
        judge.alpha = alpha;
        judge.pin = pin;
        return judge;
    }
}
