package appsolutely.dancesportchampionships.utilities;

import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtil {
    public static void copy(InputStream source, OutputStream destination) throws IOException {
        byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
        while (true) {
            int len = source.read(buffer);
            if (len == -1) {
                destination.close();
                source.close();
                return;
            }
            destination.write(buffer, 0, len);
        }
    }
}
