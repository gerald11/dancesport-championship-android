package appsolutely.dancesportchampionships.utilities;

import android.content.Context;
import android.support.v4.app.NotificationCompat.WearableExtender;
import android.support.v4.media.TransportMediator;
import android.support.v4.widget.CursorAdapter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import appsolutely.dancesportchampionships.R;

public class FeedbackView extends LinearLayout implements OnClickListener {
    private String[] customAnswers;
    private int[] dark;
    private int[] glow;
    private ImageView[] imageViews;
    private int selection;

    public FeedbackView(Context context) {
        super(context);
        this.selection = 0;
        this.customAnswers = new String[]{"", "", "", ""};
        this.dark = new int[]{R.drawable.verysatisfied_dark, R.drawable.satisfied_dark, R.drawable.dissatisfied_dark, R.drawable.verydissatisfied_dark};
        this.glow = new int[]{R.drawable.verysatisfied_glow, R.drawable.satisfied_glow, R.drawable.dissatisfied_glow, R.drawable.verydissatisfied_glow};
        init();
    }

    public FeedbackView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.selection = 0;
        this.customAnswers = new String[]{"", "", "", ""};
        this.dark = new int[]{R.drawable.verysatisfied_dark, R.drawable.satisfied_dark, R.drawable.dissatisfied_dark, R.drawable.verydissatisfied_dark};
        this.glow = new int[]{R.drawable.verysatisfied_glow, R.drawable.satisfied_glow, R.drawable.dissatisfied_glow, R.drawable.verydissatisfied_glow};
        init();
    }

    public FeedbackView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.selection = 0;
        this.customAnswers = new String[]{"", "", "", ""};
        this.dark = new int[]{R.drawable.verysatisfied_dark, R.drawable.satisfied_dark, R.drawable.dissatisfied_dark, R.drawable.verydissatisfied_dark};
        this.glow = new int[]{R.drawable.verysatisfied_glow, R.drawable.satisfied_glow, R.drawable.dissatisfied_glow, R.drawable.verydissatisfied_glow};
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.feedbackoptions, this, true);
        this.imageViews = new ImageView[]{(ImageView) findViewById(R.id.verysatisfied), (ImageView) findViewById(R.id.satisfied), (ImageView) findViewById(R.id.dissatisfied), (ImageView) findViewById(R.id.verydissatisfied)};
        for (ImageView imageView : this.imageViews) {
            imageView.setOnClickListener(this);
        }
    }

    public void setCustomAnswer(String a, String b, String c, String d) {
        if (a != null) {
            this.customAnswers[0] = a;
        }
        if (b != null) {
            this.customAnswers[1] = b;
        }
        if (c != null) {
            this.customAnswers[2] = c;
        }
        if (d != null) {
            this.customAnswers[3] = d;
        }
    }

    public int getSelection() {
        return this.selection;
    }

    public String getAnswer() {
        if (this.selection > 0 && this.selection <= 4 && !this.customAnswers[this.selection - 1].isEmpty()) {
            return this.customAnswers[this.selection - 1];
        }
        switch (this.selection) {
            case 1:
                return "Very Dissatisfied";
            case 2:
                return "Dissatisfied";
            case 3:
                return "Satisfied";
            case 4:
                return "Very Satisfied";
            default:
                return "Not answered";
        }
    }

    public void disable(int[] selections) {
        for (int selection : selections) {
            if (selection < this.imageViews.length && selection >= 0) {
                this.imageViews[selection].setVisibility(View.GONE);
            }
        }
    }

    public void enable(int[] selections) {
        for (int selection : selections) {
            if (selection < this.imageViews.length && selection >= 0) {
                this.imageViews[selection].setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClick(View v) {
        for (int i = 0; i < this.imageViews.length; i++) {
            if (this.imageViews[i].getId() == v.getId()) {
                this.imageViews[i].setImageResource(this.glow[i]);
                this.selection = i + 1;
            } else {
                this.imageViews[i].setImageResource(this.dark[i]);
            }
        }
    }
}
