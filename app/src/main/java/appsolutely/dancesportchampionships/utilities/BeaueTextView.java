package appsolutely.dancesportchampionships.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class BeaueTextView extends TextView {
    public BeaueTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BeaueTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BeaueTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "BEAUE_.otf"));
    }
}
