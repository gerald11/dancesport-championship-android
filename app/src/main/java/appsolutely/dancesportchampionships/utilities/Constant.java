package appsolutely.dancesportchampionships.utilities;

public class Constant {

    public class Urls {
        //changed adjudicator listing and confirmation listing
        public static final String ADJUDICATOR_LISTING = "https://www.dropbox.com/s/yckgl6rrhx7xr1s/Adjudicators%20Listing.csv?dl=0";
        public static final String ADVERTS = "https://www.dropbox.com/s/pxmxgzoi0kxxnx1/Adverts.txt?dl=0";
        public static final String CHAMPIONSHIP_NAME = "https://www.dropbox.com/s/om9se85tmcutqhc/ChampionshipName.txt?dl=0";
        public static final String CLOSING_DATE = "https://www.dropbox.com/s/325gxf6wuszdhae/ClosingDate.txt?dl=0";
        public static final String CONFIRMATION_FOOTER = "https://www.dropbox.com/s/7edoykd7qwd01hm/Confirmationfooter.txt?dl=0";
        public static final String CONFIRMATION_LISTING = "https://www.dropbox.com/s/ghf1gbtg2b24xu0/Confirmation%20Listing.csv?dl=0";
        public static final String CONFIRMATION_PARA1 = "https://www.dropbox.com/s/7vtra0gvx9lfx4z/ConfirmationPara1.txt?dl=0";
        public static final String CONFIRMATION_PARA2 = "https://www.dropbox.com/s/rxny4s3ql4cibgc/ConfirmationPara2.txt?dl=0";
        public static final String CONFIRMATION_PARA3 = "https://www.dropbox.com/s/qzq2jl9r1s0x9uc/ConfirmationPara3Payment.txt?dl=0";
        public static final String CONFIRMATION_PARA4 = "https://www.dropbox.com/s/hkkd0zrc1u2rsns/ConfirmationPara4.txt?dl=0";
        private static final String CONTEXT = "sh/gt8sl9bzr06vlf4";
        public static final String DANCESCORE_URLS = "https://www.dropbox.com/s/ur8buf2jrsc1s93/DanceScoreURL.txt?dl=0";
        public static final String DATE = "https://www.dropbox.com/s/ncl6x90jh7ppvgs/Date.txt?dl=0";
        public static final String DETAILS_LATIN = "https://www.dropbox.com/sh/gt8sl9bzr06vlf4/AAABqz82AdG6-x8xD0ZS93N2a/DetailsLatin.txt";
        public static final String DETAILS_STANDARD = "https://www.dropbox.com/sh/gt8sl9bzr06vlf4/AABBfSVXC_8TtVjb6nEm7KEEa/DetailsStandard.txt";
        public static final String ENTRY_FEES = "https://www.dropbox.com/s/c0hshq6vf13xslc/EntryFees.txt?dl=0";
        public static final String EVENTS_LATIN = "https://www.dropbox.com/s/y49d01brl03kpqs/EventsLatin.txt?dl=0";
        public static final String EVENTS_LIST = "https://www.dropbox.com/s/6tn9zpwpnc5hd1j/Event%20Listing.csv?dl=0";
        public static final String EVENTS_STANDARD = "https://www.dropbox.com/s/gd2r2wmo18o509t/EventsStandard.txt?dl=0";
        //public static final String EVENT_LOGO = "https://www.dropbox.com/sh/gt8sl9bzr06vlf4/AACEBZN3PQ7skmjJqjkh8qk5a?dl=0&preview=EventLogo.jpg";
        private static final String HOST = "https://www.dropbox.com";
        public static final String INFO = "https://www.dropbox.com/s/02299hzvc69x15r/Info.txt?dl=0";
        private static final String METHOD_ADJUDICATOR_LISTING = "AAARtOlcJStzNUBO1kHUHUsia/Adjudicators%20Listing.csv";
        private static final String METHOD_ADVERTS = "AAAtcJVPC46YFW-zfVUvE9DAa/Adverts.txt";
        private static final String METHOD_CHAMPIONSHIP_NAME = "AAA2BgSmDniMiFo02eA2SUWAa/ChampionshipName.txt";
        private static final String METHOD_CLOSING_DATE = "AAD0_wJFgbo3aVtV_nlYcG5za/ClosingDate.txt";
        private static final String METHOD_CONFIRMATION_FOOTER = "AAD3nfHyXyDURIV_RzfoIbdva/Confirmationfooter.txt";
        private static final String METHOD_CONFIRMATION_LISTING = "AADLH26FQIZ6QSGlgvnPWpoBa/Confirmation%20Listing.csv";
        private static final String METHOD_CONFIRMATION_PARA1 = "AAAB3EfpEHR5VLDxEWhAoSVza/ConfirmationPara1.txt";
        private static final String METHOD_CONFIRMATION_PARA2 = "AADovMFIo1xeIXdaGZ9KoKg7a/ConfirmationPara2.txt";
        private static final String METHOD_CONFIRMATION_PARA3 = "AAAgirA1CTKiO1wPmwPDF3Zaa/ConfirmationPara3Payment.txt";
        private static final String METHOD_CONFIRMATION_PARA4 = "AACeBcbJXdAIV09SaT4DjbcAa/ConfirmationPara4.txt";
        private static final String METHOD_DANCESCORE_URLS = "AABXChO1VJdOXnqgI5MUJdd7a/DanceScoreURL.txt";
        private static final String METHOD_DATE = "AAD0z-stPbm4Dh10w-7IhKWca/Date.txt";
        private static final String METHOD_DETAILS_LATIN = "AAABqz82AdG6-x8xD0ZS93N2a/DetailsLatin.txt";
        private static final String METHOD_DETAILS_STANDARD = "AABBfSVXC_8TtVjb6nEm7KEEa/DetailsStandard.txt";
        private static final String METHOD_ENTRY_FEES = "AADzbN-Ly9Dl1rQRSWaft1o4a/Entryfees.txt";
        private static final String METHOD_EVENTS_LATIN = "AACXWCUxNULE3k_OeyzTmW-Za/EventsLatin.txt";
        private static final String METHOD_EVENTS_LIST = "AAD90wMk5k3if0KCcAP621cea/Event%20Listing.csv";
        private static final String METHOD_EVENTS_STANDARD = "AAB6uSg_Csji9mty4m6H_f02a/EventsStandard.txt";
        private static final String METHOD_EVENT_LOGO = "AACEBZN3PQ7skmjJqjkh8qk5a?dl=0&preview=EventLogo.jpg";
        private static final String METHOD_INFO = "AAAO7Emv6rt3PcTSRtwspznva/Info.txt";
        private static final String METHOD_ORDER_NOW = "AACQnfnAxRaDUjro6ROnWPrla/Ordernow.txt";
        private static final String METHOD_ORGANISER_MESSAGE = "AABxojw_BeDedAiUSi2rsrfta/OrganiserMessage.txt";
        private static final String METHOD_RULES = "AAD ySp9WK8bv0fxPa9o5q1eYa/Rules.txt";
        private static final String METHOD_SUPPORTED_BY = "AABHy5Zh3Fsv7ILICytdracba/Supportedby.txt";
        private static final String METHOD_TICKET_PRICE = "AADxuk4eNmf4FvA_hDK5b_QGa/Ticketprice.txt";
        private static final String METHOD_VENUE = "AAAylUzIugDFbOIyNeX0VW-oa/Venue.txt";
        public static final String ORDER_NOW = "https://www.dropbox.com/s/dvc394915h6wr6q/OrderNow.txt?dl=0";
        public static final String ORGANISER_MESSAGE = "https://www.dropbox.com/s/ift9bvfsul8xs07/OrganiserMessage.txt?dl=0";
        public static final String RULES = "https://www.dropbox.com/s/ozw9h201cnrb9dv/Rules.txt?dl=0";
        public static final String SUPPORTED_BY = "https://www.dropbox.com/s/kvpxs8gb0kdzskr/Supportedby.txt?dl=0";
        public static final String TICKET_PRICE = "https://www.dropbox.com/s/semhc16d9xdj0yn/TicketPrice.txt?dl=0";
        public static final String VENUE = "https://www.dropbox.com/s/y2zgh8zgujn1hku/Venue.txt?dl=0";
        public static final String EVENT_LOGO = "https://dl.dropboxusercontent.com/s/608z6omtj2awkc9/EventLogo.png?dl=0";
        public static final String ORGANISER_PICTURE   = "https://dl.dropboxusercontent.com/s/ls81vov4s7mzoy6/OrganiserPicture.jpg?dl=0";
        public static final String CONFIRMATION_RIGHT_LOGO = "https://dl.dropboxusercontent.com/s/ot8ko00g9lc427v/ConfirmationRightLogo.png?dl=0";
        public static final String ADVERT_1 = "https://dl.dropboxusercontent.com/s/brp2myzqg3t69tg/Advert1.jpg?dl=0";
        public static final String ADVERT_2 = "https://dl.dropboxusercontent.com/s/0hk405cotrujlpj/advert2.jpg?dl=0";
        public static final String ADVERT_3 = "https://dl.dropboxusercontent.com/s/nl1jwjrvcve1a1i/advert3.jpg?dl=0";

    }
}
