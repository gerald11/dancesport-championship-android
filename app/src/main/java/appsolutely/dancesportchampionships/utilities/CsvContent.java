package appsolutely.dancesportchampionships.utilities;

import java.util.ArrayList;
import java.util.List;

public class CsvContent {
    private List<String[]> data;

    public CsvContent(String content) {
        this.data = new ArrayList();
        for (String line : content.split("\n")) {
            if (line.endsWith(",")) {
                line = new StringBuilder(String.valueOf(line)).append(" ").toString();
            }
            this.data.add(line.split(","));
        }
    }

    public String get(int line, int pos) {
        try {
            return ((String[]) this.data.get(line))[pos];
        } catch (Exception e) {
            return null;
        }
    }

    public List<String[]> getData() {
        return this.data;
    }
}
