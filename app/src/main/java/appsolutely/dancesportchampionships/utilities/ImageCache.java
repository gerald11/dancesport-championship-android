package appsolutely.dancesportchampionships.utilities;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.util.SparseArray;
import android.widget.ImageView;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class ImageCache {
    private static final String RES_DRAWABLE_KEY = "res_drawable:";
    private static ImageCache instance;
    private Map<String, Drawable> cache;
    private Context context;
    private SparseArray<BackgroundLoader> reference;

    class BackgroundLoader extends AsyncTask<String, Void, Drawable> {
        private final WeakReference<ImageView> imageViewReference;

        public BackgroundLoader(ImageView imageView) {
            this.imageViewReference = new WeakReference(imageView);
        }

        protected Drawable doInBackground(String... params) {
            if (params == null || params.length < 1 || params[0] == null) {
                return null;
            }
            return load(params[0]);
        }

        protected void onPostExecute(Drawable drawable) {
            if (isCancelled()) {
                drawable = null;
            }
            ImageView imageView = (ImageView) this.imageViewReference.get();
            if (ImageCache.this.reference.get(imageView.hashCode()) != this) {
                drawable = null;
            } else {
                ImageCache.this.reference.remove(imageView.hashCode());
            }
            if (drawable != null && imageView != null) {
                ImageCache.this.setImageDrawable(imageView, drawable);
            }
        }

        private Drawable load(String path) {
            if (ImageCache.this.context == null) {
                System.err.println("Cache context null");
                return null;
            }
            try {
                Drawable drawable = Drawable.createFromStream(ImageCache.this.context.getAssets().open(path), path);
                ImageCache.this.cache.put(path, drawable);
                return drawable;
            } catch (IOException e) {
                System.err.println("Cache exception");
                e.printStackTrace();
                return null;
            }
        }
    }

    private ImageCache() {
        this.cache = new HashMap();
        this.reference = new SparseArray();
    }

    public static ImageCache getInstance(Context context) {
        if (instance == null) {
            instance = new ImageCache();
            instance.context = context;
        }
        return instance;
    }

    public void loadDrawable(String path, ImageView imageView) {
        Drawable drawable = getDrawable(path);
        imageView.setImageDrawable(null);
        if (drawable != null) {
            setImageDrawable(imageView, drawable);
            return;
        }
        BackgroundLoader loader = new BackgroundLoader(imageView);
        this.reference.put(imageView.hashCode(), loader);
        loader.execute(new String[]{path});
    }

    public Drawable getDrawable(String path) {
        if (this.cache.containsKey(path)) {
            return (Drawable) this.cache.get(path);
        }
        return null;
    }

    public Drawable getDrawable(int resId) {
        String path = new StringBuilder(RES_DRAWABLE_KEY).append(resId).toString();
        if (this.cache.containsKey(path)) {
            return (Drawable) this.cache.get(path);
        }
        if (this.context == null) {
            return null;
        }
        Drawable drawable = this.context.getResources().getDrawable(resId);
        this.cache.put(path, drawable);
        return drawable;
    }

    private void setImageDrawable(ImageView imageView, Drawable drawable) {
        TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), drawable});
        imageView.setImageDrawable(td);
        td.startTransition(300);
    }
}
