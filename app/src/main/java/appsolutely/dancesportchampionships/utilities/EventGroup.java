package appsolutely.dancesportchampionships.utilities;

import java.util.ArrayList;
import java.util.List;

public class EventGroup {
    public List<EventItem> events;
    public String name;

    public EventGroup(String name) {
        this.events = new ArrayList();
        this.name = name;
    }
}
