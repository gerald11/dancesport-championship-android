package appsolutely.dancesportchampionships.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class BeaueButton extends Button {
    public BeaueButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BeaueButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BeaueButton(Context context) {
        super(context);
        init();
    }

    private void init() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "BEAUE_.otf"));
    }
}
