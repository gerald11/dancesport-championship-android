package appsolutely.dancesportchampionships.utilities;

import appsolutely.dancesportchampionships.utilities.DownloadUtil.DownloadCallback;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataCache {
    private static Map<String, String> cache;

    public interface CacheCallback {
        void onContent(String str, String str2);
    }

    static {
        cache = new HashMap();
    }

    public static void get(final String url, final CacheCallback callback) {
        if (cache.get(url) == null || callback == null) {
            try {
                DownloadUtil.request(url, new DownloadCallback() {
                    @Override
                    public void onResult(String result) {
                        if (result != null) {
                            DataCache.cache.put(url, result);
                            if (callback != null) {
                                callback.onContent(url, result);
                            }
                        }
                    }
                });
                return;
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        callback.onContent(url, cache.get(url));
    }
}
