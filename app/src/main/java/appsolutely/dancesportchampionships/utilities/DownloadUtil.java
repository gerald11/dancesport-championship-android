package appsolutely.dancesportchampionships.utilities;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadUtil {
    public static final String URL_RULES = "https://www.dropbox.com/sh/gt8sl9bzr06vlf4/AADySp9WK8bv0fxPa9o5q1eYa/Rules.txt";

    public interface DownloadCallback {
        void onResult(String str);
    }

    private static class Downloader extends AsyncTask<Void, Void, String> {
        DownloadCallback callback;
        String filename;
        String url;

        public Downloader(String url, DownloadCallback callback) {
            this.url = null;
            this.filename = null;
            this.callback = null;
            this.url = url;
            this.callback = callback;
        }

        public Downloader(String url, String filename, DownloadCallback callback) {
            this.url = null;
            this.filename = null;
            this.callback = null;
            this.url = url;
            this.callback = callback;
        }

        protected String doInBackground(Void... params) {
            try {
                HttpURLConnection con = (HttpURLConnection) new URL(this.url).openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("user-agent", "Wget/1.15 (linux-gnu)");
                con.setDoOutput(false);
                con.setReadTimeout(5000);
                con.setConnectTimeout(5000);
                con.connect();
                if (this.filename == null || this.filename.isEmpty()) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String outString = "";
                    while (true) {
                        String inputLine = in.readLine();
                        if (inputLine == null) {
                            in.close();
                            con.disconnect();
                            return outString;
                        }
                        outString = new StringBuilder(String.valueOf(outString)).append(inputLine).append("\n").toString();
                    }
                } else {
                    File file = new File(this.filename);
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileUtil.copy(con.getInputStream(), new FileOutputStream(file));
                    return this.filename;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "Error";
            }
        }

        protected void onPostExecute(String result) {
            if (this.callback != null) {
                this.callback.onResult(result);
            }
        }
    }

    public static void request(String url, DownloadCallback callback) throws IOException {
        new Downloader(url, callback).execute(new Void[0]);
    }

    public static void request(String url, String filename, DownloadCallback callback) throws IOException {
        new Downloader(url, filename, callback).execute(new Void[0]);
    }
}
