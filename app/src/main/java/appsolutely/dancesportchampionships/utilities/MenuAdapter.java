package appsolutely.dancesportchampionships.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Map;

import appsolutely.dancesportchampionships.R;

public class MenuAdapter extends ArrayAdapter<String> {
    private Map<String, Integer> directory;

    public MenuAdapter(Context context, int resource, Map<String, Integer> directory, String[] order) {
        super(context, resource, order);
        this.directory = directory;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_menu, parent, false);
        }
        String name = getItem(position);
        ((TextView) convertView.findViewById(R.id.text)).setText(name);
        ((ImageView) convertView.findViewById(R.id.image)).setImageResource(directory.get(name));
        return convertView;
    }
}
