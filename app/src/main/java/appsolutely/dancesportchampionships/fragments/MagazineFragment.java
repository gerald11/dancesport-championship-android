package appsolutely.dancesportchampionships.fragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.File;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;

public class MagazineFragment extends AdvertFragment {
    public static HomeFragment fragmentId;
    private View rootView;
    private WebView wvMagazine;
    private ProgressDialog pDialog;

    static {
        fragmentId = HomeFragment.MAGAZINE;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_magazine, container, false);
      //  initializeAdvert(this.rootView);
        ((HomeActivity) getActivity()).beaueTitle(HomeFragment.getName(fragmentId), true);
        ((HomeActivity) getActivity()).highlightTab(fragmentId);
        ((HomeActivity) getActivity()).setBackContext(HomeFragment.getName(fragmentId));

        wvMagazine = (WebView) rootView.findViewById(R.id.wvMagazine);
        wvMagazine.getSettings().setJavaScriptEnabled(true);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("PDF");
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        try{
            wvMagazine.loadUrl("https://www.dropbox.com/s/9d2svgyjub0uhcv/Brillante_Magazine.pdf?dl=0&m=");
            listener();
        }
        catch (Exception e){

        }

   /*     File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +"/brillantemagazine.pdf");
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Log.i("string1", "string2");
        }*/

        return this.rootView;
    }

    private void listener() {
        wvMagazine.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pDialog.dismiss();
            }
        });
    }


//    private void copyAssets() {
//        AssetManager assetManager = this.getAssets();
//        String[] files = null;
//        try {
//            files = assetManager.list("");
//        } catch (IOException e) {
//            Log.e("tag", "Failed to get asset file list.", e);
//        }
//        if (files != null) for (String filename : files) {
//            InputStream in = null;
//            OutputStream out = null;
//            try {
//                in = assetManager.open(filename);
//                File outFile = new File(getExternalFilesDir(null), filename);
//                out = new FileOutputStream(outFile);
//                copyFile(in, out);
//            } catch(IOException e) {
//                Log.e("tag", "Failed to copy asset file: " + filename, e);
//            }
//            finally {
//                if (in != null) {
//                    try {
//                        in.close();
//                    } catch (IOException e) {
//                        // NOOP
//                    }
//                }
//                if (out != null) {
//                    try {
//                        out.close();
//                    } catch (IOException e) {
//                        // NOOP
//                    }
//                }
//            }
//        }
//    }
//    private void copyFile(InputStream in, OutputStream out) throws IOException {
//        byte[] buffer = new byte[1024];
//        int read;
//        while((read = in.read(buffer)) != -1){
//            out.write(buffer, 0, read);
//        }
//    }
}