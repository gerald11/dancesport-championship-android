package appsolutely.dancesportchampionships.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.MenuAdapter;
import java.util.HashMap;
import java.util.Map;

public class AudienceFragment extends AdvertFragment {
    public static HomeFragment fragmentId;
    private View rootView;

    static {
        fragmentId = HomeFragment.AUDIENCE;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_audience, container, false);
        initializeAdvert(this.rootView);
        initializeMenu(getDirectory(), getOrder(), this.rootView);
        ((HomeActivity) getActivity()).beaueTitle(HomeFragment.getName(fragmentId), true);
        ((HomeActivity) getActivity()).highlightTab(fragmentId);
        ((HomeActivity) getActivity()).setBackContext(HomeFragment.getName(fragmentId));
        return this.rootView;
    }

    protected void initializeMenu(Map<String, Integer> directory, String[] order, View rootView) {
        ListView mv = (ListView) rootView.findViewById(R.id.menu_list);
        mv.setAdapter(new MenuAdapter(getActivity(), 0, directory, order));
        mv.setOnItemClickListener((HomeActivity) getActivity());
    }

    private String[] getOrder() {
        return new String[]{"Tickets", "Organiser's Message", "Events List", "Live Programme", "Live Recall", "Results", "Feedback"};
    }

    private Map<String, Integer> getDirectory() {
        Map<String, Integer> directory = new HashMap();
        directory.put("Tickets", R.drawable.menu_tickets);
        directory.put("Organiser's Message", R.drawable.menu_liverecall);
        directory.put("Events List", R.drawable.menu_workshop);
        directory.put("Live Programme", R.drawable.menu_liveprogramme);
        directory.put("Live Recall", R.drawable.menu_liverecall);
        directory.put("Results", R.drawable.menu_results);
        directory.put("Feedback", R.drawable.menu_feedback);
        return directory;
    }
}
