package appsolutely.dancesportchampionships.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.DownloadUtil.DownloadCallback;

public class TextFragment extends Fragment implements DownloadCallback {
    private String backTitle;
    private String body;
    private View rootView;
    private String title;

    public static TextFragment instance(String title, String backTitle) {
        TextFragment fragment = new TextFragment();
        fragment.title = title;
        fragment.body = "Loading...";
        fragment.backTitle = backTitle;
        return fragment;
    }

    public void onResult(String content) {
        TextView bodyElem = (TextView) this.rootView.findViewById(R.id.text_body);
        if (this.rootView != null && bodyElem != null) {
            bodyElem.setText(content);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int i;
        this.rootView = inflater.inflate(R.layout.fragment_text, container, false);
        ((HomeActivity) getActivity()).backTitle(this.backTitle);
        TextView titleElem = (TextView) this.rootView.findViewById(R.id.text_title);
        titleElem.setVisibility(title.isEmpty() ? View.GONE : View.VISIBLE);
        titleElem.setText(this.title);
        ((TextView) this.rootView.findViewById(R.id.text_body)).setText(this.body);
        return this.rootView;
    }
}
