package appsolutely.dancesportchampionships.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.MenuAdapter;
import java.util.HashMap;
import java.util.Map;

public class CompetitorsFragment extends AdvertFragment {
    public static HomeFragment fragmentId;
    private View rootView;

    static {
        fragmentId = HomeFragment.COMPETITORS;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_competitors, container, false);
        initializeMenu(getDirectory(), getOrder(), this.rootView);
        initializeAdvert(this.rootView);
        ((HomeActivity) getActivity()).beaueTitle(HomeFragment.getName(fragmentId), true);
        ((HomeActivity) getActivity()).highlightTab(fragmentId);
        ((HomeActivity) getActivity()).setBackContext(HomeFragment.getName(fragmentId));
        return this.rootView;
    }

    protected void initializeMenu(Map<String, Integer> directory, String[] order, View rootView) {
        ListView mv = (ListView) rootView.findViewById(R.id.menu_list);
        mv.setAdapter(new MenuAdapter(getActivity(), 0, directory, order));
        mv.setOnItemClickListener((HomeActivity) getActivity());
    }

    private String[] getOrder() {
        return new String[]{"Events", "Rules & Regulations", "Registration", "Confirmation", "Competitor List", "Live Programme", "Live Recall", "Results", "Feedback"};
    }

    private Map<String, Integer> getDirectory() {
        Map<String, Integer> directory = new HashMap();
        directory.put("Events", Integer.valueOf(R.drawable.menu_events));
        directory.put("Rules & Regulations", Integer.valueOf(R.drawable.menu_rulesregulations));
        directory.put("Registration", Integer.valueOf(R.drawable.menu_registration));
        directory.put("Confirmation", Integer.valueOf(R.drawable.menu_confirmation));
        directory.put("Live Programme", Integer.valueOf(R.drawable.menu_liveprogramme));
        directory.put("Live Recall", Integer.valueOf(R.drawable.menu_liverecall));
        directory.put("Results", Integer.valueOf(R.drawable.menu_results));
        directory.put("Competitor List", Integer.valueOf(R.drawable.menu_workshop));
        directory.put("Feedback", Integer.valueOf(R.drawable.menu_feedback));
        return directory;
    }
}
