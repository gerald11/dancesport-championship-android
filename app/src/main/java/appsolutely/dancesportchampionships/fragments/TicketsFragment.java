package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;

public class TicketsFragment extends Fragment implements OnClickListener, CacheCallback {
    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_tickets, container, false);
        this.rootView.findViewById(R.id.done).setOnClickListener(this);
        ((HomeActivity) getActivity()).backTitle(HomeFragment.getName(HomeFragment.AUDIENCE));
        initializeTickets();
        return this.rootView;
    }

    private void initializeTickets() {
        DataCache.get(Urls.DATE, this);
        DataCache.get(Urls.VENUE, this);
        DataCache.get(Urls.TICKET_PRICE, this);
        DataCache.get(Urls.ORDER_NOW, this);
    }

    public void onContent(String url, String content) {
        if (this.rootView != null) {
            if (url.equals(Urls.DATE)) {
                onDateContent(content);
            } else if (url.equals(Urls.VENUE)) {
                onVenueContent(content);
            } else if (url.equals(Urls.TICKET_PRICE)) {
                onPriceContent(content);
            } else if (url.equals(Urls.ORDER_NOW)) {
                onOrdernowContent(content);
            }
        }
    }

    private void onDateContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.tickets_date_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onVenueContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.tickets_venue_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onPriceContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.tickets_price_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onOrdernowContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.tickets_ordernow_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                submitOrder();
            default:
        }
    }

    private void submitOrder() {
        String inputDayTickets = ((EditText) this.rootView.findViewById(R.id.input_daytickets)).getText().toString();
        String content = getString(R.string.tickets_body, new Object[]{HomeActivity.championshipname, inputDayTickets});
        ((HomeActivity) getActivity()).mail(getString(R.string.tickets_email).split(";"), getString(R.string.tickets_subject, new Object[]{HomeActivity.championshipname}), content);
    }
}
