package appsolutely.dancesportchampionships.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;

public class WebViewFragment extends Fragment {
    private String backTitle;
    private String contentUrl;
    private View progress;
    private View rootView;
    private WebView webView;

    public interface LinkListener {
        void link(String str, String str2);
    }

    public static WebViewFragment instance(String url, String backTitle) {
        WebViewFragment fragment = new WebViewFragment();
        fragment.contentUrl = url;
        fragment.backTitle = backTitle;
        return fragment;
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        ((HomeActivity) getActivity()).backTitle(this.backTitle);
        this.webView = (WebView) this.rootView.findViewById(R.id.webview);
        this.progress = this.rootView.findViewById(R.id.progress);
        this.webView.getSettings().setSupportZoom(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.getSettings().setDisplayZoomControls(false);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setBackgroundColor(0);
        this.webView.loadUrl(this.contentUrl);
        this.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if (WebViewFragment.this.progress.getVisibility() != View.GONE) {
                    WebViewFragment.this.progress.setVisibility(View.GONE);
                }
                super.onPageFinished(view, url);
            }
        });
        return this.rootView;
    }
}
