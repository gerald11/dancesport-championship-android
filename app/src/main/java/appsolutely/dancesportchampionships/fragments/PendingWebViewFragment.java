package appsolutely.dancesportchampionships.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.CsvContent;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;

public class PendingWebViewFragment extends Fragment implements CacheCallback {
    private String backTitle;
    private String contentUrl;
    private int index;
    private View progress;
    private View rootView;
    private WebView webView;

    public static PendingWebViewFragment instance(String url, int index, String backTitle) {
        PendingWebViewFragment fragment = new PendingWebViewFragment();
        fragment.index = 0;
        fragment.contentUrl = url;
        fragment.index = index;
        fragment.backTitle = backTitle;
        return fragment;
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        ((HomeActivity) getActivity()).backTitle(this.backTitle);
        this.webView = (WebView) this.rootView.findViewById(R.id.webview);
        this.progress = this.rootView.findViewById(R.id.progress);
        this.webView.getSettings().setSupportZoom(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.getSettings().setDisplayZoomControls(false);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setBackgroundColor(0);
        this.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if (progress.getVisibility() != View.GONE) {
                    progress.setVisibility(View.GONE);
                }
                super.onPageFinished(view, url);
            }
        });
        DataCache.get(this.contentUrl, this);
        return this.rootView;
    }

    public void onContent(String url, String content) {
        CsvContent csv = new CsvContent(content);
        if (this.webView != null && csv.get(this.index, 0) != null) {
            this.webView.loadUrl(csv.get(this.index, 0));
        }
    }
}
