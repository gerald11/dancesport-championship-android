package appsolutely.dancesportchampionships.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.CsvContent;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;
import appsolutely.dancesportchampionships.utilities.Judge;
import java.util.ArrayList;
import java.util.List;

public class AdjudicatorsFragment extends Fragment implements CacheCallback, OnClickListener {
    public static HomeFragment fragmentId;
    private GalleryAdapter adapter;
    private OnSelectJudge listener;
    private View rootView;

    public interface OnSelectJudge {
        void judge(int i);
    }

    static {
        fragmentId = HomeFragment.ADJUCATORS;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public AdjudicatorsFragment() {
    }

    public void setListener(OnSelectJudge listener) {
        this.listener = listener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_adjudicators, container, false);
        ((HomeActivity) getActivity()).beaueTitle(HomeFragment.getName(fragmentId), false);
        ((HomeActivity) getActivity()).highlightTab(fragmentId);
        adapter = new GalleryAdapter(getActivity(), 0, new ArrayList(), listener);
        initializeGallery();
        DataCache.get(Urls.ADJUDICATOR_LISTING, this);
        return rootView;
    }

    public void onClick(View v) {
    }

    public void onContent(String url, String result) {
        if (rootView != null) {
            CsvContent content = new CsvContent(result);
            adapter.clear();
            List<String[]> judgesContent = content.getData();
            for (int i = 0; i < judgesContent.size(); i++) {
                String[] data = judgesContent.get(i);
                adapter.add(Judge.m7n(i, data[2], data[0], data[1], "", Character.toString((char) (i + 65)), data[3]));
            }
            if (rootView != null && rootView.findViewById(R.id.progress) != null) {
                rootView.findViewById(R.id.progress).setVisibility(View.GONE);
            }
        }
    }

    private void initializeGallery() {
        ListView lv = (ListView) rootView.findViewById(R.id.gallery);
        getActivity().getWindowManager().getDefaultDisplay().getSize(new Point());
        lv.setAdapter(adapter);
    }

    class GalleryAdapter extends ArrayAdapter<Judge> {
        private OnSelectJudge listener;

        public GalleryAdapter(Context context, int resource, List<Judge> objects, OnSelectJudge listener) {
            super(context, resource, objects);
            this.listener = listener;
        }

        public int getCount() {
            return super.getCount();
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_gallery, (LinearLayout) convertView, false);
            }
            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.judge(position);
                    }
                }
            });
            Judge judge = getItem(position);
            ((TextView) convertView.findViewById(R.id.textView3)).setText(judge.alpha);
            ((TextView) convertView.findViewById(R.id.textView1)).setText(judge.name);
            ((TextView) convertView.findViewById(R.id.textView2)).setText(judge.country);
            return convertView;
        }
    }
}
