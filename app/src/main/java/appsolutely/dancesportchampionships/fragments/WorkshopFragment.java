package appsolutely.dancesportchampionships.fragments;

import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;

public class WorkshopFragment extends Fragment implements OnClickListener {
    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_workshop, container, false);
        this.rootView.findViewById(R.id.done).setOnClickListener(this);
        ((HomeActivity) getActivity()).backTitle(HomeFragment.getName(HomeFragment.COMPETITORS));
        return this.rootView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                submitRegistration();
            default:
        }
    }

    private void submitRegistration() {
        String name = ((EditText) this.rootView.findViewById(R.id.input_name)).getText().toString();
        String email = ((EditText) this.rootView.findViewById(R.id.input_email)).getText().toString();
        String phone = ((EditText) this.rootView.findViewById(R.id.input_phone)).getText().toString();
        if (name.isEmpty() || email.isEmpty() || phone.isEmpty()) {
            new Builder(getActivity()).setTitle("Please fill in everything!")
                    .setMessage("Please fill in all the fields to register! :)")
                    .setNeutralButton("Ok", null).show();
            return;
        }
        ((HomeActivity) getActivity()).mail(getString(R.string.workshop_email).split(";"), getString(R.string.workshop_subject), getActivity().getString(R.string.workshop_body, new Object[]{name, email, phone}));
    }
}
