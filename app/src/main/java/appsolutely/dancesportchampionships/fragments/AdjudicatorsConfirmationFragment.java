package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.CsvContent;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;
import appsolutely.dancesportchampionships.utilities.Judge;
import java.util.ArrayList;
import java.util.List;

public class AdjudicatorsConfirmationFragment extends Fragment implements OnClickListener, CacheCallback {
    public static HomeFragment fragmentId = HomeFragment.ADJUDICATOR_CONFIRM;
    private int index;
    private List<Judge> judges = new ArrayList();
    private View rootView;

    public AdjudicatorsConfirmationFragment() {
    }

    public void setIndex(int position) {
        index = position;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_adjconfirmation, container, false);
        rootView.findViewById(R.id.done).setOnClickListener(this);
        ((EditText) rootView.findViewById(R.id.input_pin)).setText(((HomeActivity) getActivity()).getPref().getString("judge_code", ""));
        ((HomeActivity) getActivity()).beaueTitle(HomeFragment.getName(fragmentId), true);
        ((HomeActivity) getActivity()).highlightTab(fragmentId);
        ((HomeActivity) getActivity()).setBackContext(HomeFragment.getName(fragmentId));
        loadJudges();
        return rootView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                checkPin();
            default:
        }
    }

    public void onContent(String url, String result) {
        List<String[]> judgesContent = new CsvContent(result).getData();
        for (int i = 0; i < judgesContent.size(); i++) {
            String[] data = judgesContent.get(i);
            judges.add(Judge.m7n(i, data[3], data[0], data[1], "", Character.toString((char) (i + 65)), data[2]));
        }
        if (rootView != null && rootView.findViewById(R.id.confirmation_overlay) != null) {
            rootView.findViewById(R.id.input_pin).setFocusableInTouchMode(true);
            rootView.findViewById(R.id.input_pin).setFocusable(true);
            rootView.findViewById(R.id.confirmation_overlay).setVisibility(View.GONE);
        }
    }

    private void loadJudges() {
        DataCache.get(Urls.ADJUDICATOR_LISTING, this);
    }

    private void checkPin() {
        String pin = ((EditText) rootView.findViewById(R.id.input_pin)).getText().toString();
        if (pin.length() == 0) {
            Toast.makeText(getActivity(), "Please enter 3 digits", Toast.LENGTH_LONG).show();
            return;
        }
        Judge judge = judges.get(index);
        if (judge.pin.equals(pin)) {
            ((HomeActivity) getActivity()).getPref().edit().putString("judge_code", pin).commit();
            ((HomeActivity) getActivity()).judge(judge);
            return;
        }
        Toast.makeText(getActivity(), "Incorrect PIN", Toast.LENGTH_LONG).show();
    }
}
