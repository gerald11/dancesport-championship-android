package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;
import appsolutely.dancesportchampionships.utilities.Participant;

public class ConfirmationLetterFragment extends Fragment implements CacheCallback {
    private Participant participant;
    private View rootView;

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_confirmationletter, container, false);
        ((HomeActivity) getActivity()).backTitle("Back");
        initializeLetter();

        ImageView imgViewLogoLeft = (ImageView)rootView.findViewById(R.id.imgViewLogoLeft);
        Picasso.get()
                .load(Constant.Urls.EVENT_LOGO)
                .into(imgViewLogoLeft);

        ImageView imgViewLogoRight = (ImageView)rootView.findViewById(R.id.imgViewLogoRight);
        Picasso.get()
                .load(Urls.CONFIRMATION_RIGHT_LOGO)
                .into(imgViewLogoRight);
        return this.rootView;
    }

    private void initializeLetter() {
        LayoutInflater.from(getActivity()).inflate(R.layout.item_confirmationletter, (LinearLayout) this.rootView.findViewById(R.id.letter_container), true);
        String details = getString(R.string.confirmationletter_participant_details, this.participant.toArguments());
        DataCache.get(Urls.CONFIRMATION_PARA1, this);
        DataCache.get(Urls.CONFIRMATION_PARA2, this);
        DataCache.get(Urls.CONFIRMATION_PARA4, this);
        DataCache.get(Urls.CONFIRMATION_FOOTER, this);
        if (!this.participant.paid.equalsIgnoreCase("Y")) {
            DataCache.get(Urls.CONFIRMATION_PARA3, this);
        }
        ((TextView) this.rootView.findViewById(R.id.confirmationletter_recipient_textview)).setText("Dear " + this.participant.name + ",");
        ((TextView) this.rootView.findViewById(R.id.confirmationletter_participant_textview)).setText(details);
    }

    public void onContent(String url, String content) {
        if (this.rootView != null) {
            if (url.equals(Urls.CONFIRMATION_FOOTER)) {
                onFooterContent(content);
            } else if (url.equals(Urls.CONFIRMATION_PARA1)) {
                onPara1Content(content);
            } else if (url.equals(Urls.CONFIRMATION_PARA2)) {
                onPara2Content(content);
            } else if (url.equals(Urls.CONFIRMATION_PARA3)) {
                onPara3Content(content);
            } else if (url.equals(Urls.CONFIRMATION_PARA4)) {
                onPara4Content(content);
            }
        }
    }

    private void onPara1Content(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.confirmationletter_para1_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onPara2Content(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.confirmationletter_para2_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onPara3Content(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.confirmationletter_payment_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onPara4Content(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.confirmationletter_signoff_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onFooterContent(String content) {
        String[] footer = content.contains("\n") ? content.split("\n") : new String[]{content};
        if (footer.length > 0) {
            TextView textView = (TextView) this.rootView.findViewById(R.id.confirmationletter_footer_1);
            textView.setVisibility(View.VISIBLE);
            textView.setText(footer[0]);
        }
        if (footer.length > 1) {
            TextView textView = (TextView) this.rootView.findViewById(R.id.confirmationletter_footer_2);
            textView.setVisibility(View.VISIBLE);
            textView.setText(footer[1]);
        }
        if (footer.length > 2) {
            TextView textView = (TextView) this.rootView.findViewById(R.id.confirmationletter_footer_3);
            textView.setVisibility(View.VISIBLE);
            textView.setText(footer[2]);
        }
    }
}
