package appsolutely.dancesportchampionships.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ViewSwitcher.ViewFactory;

import com.squareup.picasso.Picasso;

import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant;
import appsolutely.dancesportchampionships.utilities.ImageCache;
import java.util.Timer;
import java.util.TimerTask;

public abstract class AdvertFragment extends Fragment implements ViewFactory {
    private static int[] advertImages;
    //private static String[] advertImages;
    private int advertCount;
    private ImageCache imageCache;
    private ImageSwitcher imageSwitcher;
    private Timer timer;

    public AdvertFragment() {
        advertCount = 0;
    }

    public void onAttach(Activity activity) {
        imageCache = ImageCache.getInstance(activity.getApplicationContext());
        super.onAttach(activity);
    }

    static {
        advertImages = new int[]{R.drawable.advert1, R.drawable.advert2, R.drawable.advert3, R.drawable.advert4, R.drawable.advert5, R.drawable.advert6, R.drawable.advert7, R.drawable.advert8};
      //  advertImages = new String[]{Constant.Urls.ADVERT_1, Constant.Urls.ADVERT_2, Constant.Urls.ADVERT_3};
    }

    protected int getNextAdvertImage() {
        int i = advertCount + 1;
        advertCount = i;
        if (i == advertImages.length) {
            advertCount = 0;
        }
        return advertImages[advertCount];
    }

   /* protected String getNextAdvertImage() {
        int i = advertCount + 1;
        advertCount = i;
        if (i == advertImages.length) {
            advertCount = 0;
        }
        return advertImages[advertCount];
    }*/

    public void onDestroyView() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onDestroyView();
    }

    public View makeView() {
        ImageView imageView = new ImageView(getActivity());
        imageView.setBackgroundColor(-1);
        imageView.setScaleType(ScaleType.FIT_XY);
        imageView.setLayoutParams(new LayoutParams(-1, -2));
        return imageView;
    }

    protected void initializeAdvert(View rootView) {
        int nextAdvertImage = getNextAdvertImage();
        imageSwitcher = (ImageSwitcher) rootView.findViewById(R.id.advertslider);
        imageSwitcher.setFactory(this);
        imageSwitcher.setImageResource(nextAdvertImage);
        startAdvertTimer();
    }

    protected void startAdvertTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Context context = getActivity();
                if (context == null) {
                    return;
                }

                Handler mainHandler = new Handler(context.getMainLooper());

                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        imageSwitcher.setImageDrawable(imageCache.getDrawable(getNextAdvertImage()));
                    }
                };
                mainHandler.post(myRunnable);
            }
        }, 0, 5000);
    }
}
