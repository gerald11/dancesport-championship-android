package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.fragments.WebViewFragment.LinkListener;

public class InfoFragment extends Fragment {
    public static HomeFragment fragmentId = HomeFragment.INFO;
    private LinkListener listener;
    private View rootView;

    public void setListener(LinkListener listener) {
        this.listener = listener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_info, container, false);
        ((HomeActivity) getActivity()).beaueTitle(HomeFragment.getName(fragmentId), false);
        ((HomeActivity) getActivity()).highlightTab(fragmentId);
        this.rootView.findViewById(R.id.appsolutely).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.link("http://www.thedancesportacademy.com/", "Info");
                }
            }
        });
        return this.rootView;
    }
}
