package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;

public class EventsFragment extends Fragment implements CacheCallback {
    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_events, container, false);
        ((HomeActivity) getActivity()).backTitle("Competitors");
        initializeEvents();
        return this.rootView;
    }

    private void initializeEvents() {
        if (this.rootView != null) {
            DataCache.get(Urls.DATE, this);
            DataCache.get(Urls.VENUE, this);
            DataCache.get(Urls.EVENTS_LATIN, this);
            DataCache.get(Urls.EVENTS_STANDARD, this);
            DataCache.get(Urls.ENTRY_FEES, this);
            DataCache.get(Urls.CLOSING_DATE, this);
        }
    }

    public void onContent(String url, String content) {
        if (url.equals(Urls.DATE)) {
            onDateContent(content);
        } else if (url.equals(Urls.VENUE)) {
            onVenueContent(content);
        } else if (url.equals(Urls.EVENTS_LATIN)) {
            onLatinContent(content);
        } else if (url.equals(Urls.EVENTS_STANDARD)) {
            onStandardContent(content);
        } else if (url.equals(Urls.ENTRY_FEES)) {
            onFeesContent(content);
        } else if (url.equals(Urls.CLOSING_DATE)) {
            onClosingContent(content);
        }
    }

    private void onVenueContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.events_venue_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onLatinContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.events_eventslatin_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onStandardContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.events_eventsstandard_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onFeesContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.events_fees_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onDateContent(String content) {
        TextView textView = (TextView) this.rootView.findViewById(R.id.events_date_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(content);
    }

    private void onClosingContent(String content) {
        String[] data = content.contains("\n") ? new String[]{content.substring(0, content.indexOf("\n")), content.substring(content.indexOf("\n") + 1)} : new String[]{"", content};
        TextView textView = (TextView) this.rootView.findViewById(R.id.events_closingdate_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(data[0]);
        textView = (TextView) this.rootView.findViewById(R.id.events_closing_textview);
        textView.setVisibility(View.VISIBLE);
        textView.setText(data[1]);
    }
}
