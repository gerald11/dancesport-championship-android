package appsolutely.dancesportchampionships.fragments;

import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.FeedbackView;

public class FeedbackFragment extends Fragment implements OnClickListener {
    private String backTitle;
    private View rootView;

    public void setTitle(String backTitle) {
        this.backTitle = backTitle;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_feedback, container, false);
        ((FeedbackView) this.rootView.findViewById(R.id.feedback4)).disable(new int[]{1, 2});
        ((FeedbackView) this.rootView.findViewById(R.id.feedback1)).setCustomAnswer("Very Good", "Good", "Bad", "Very Bad");
        ((FeedbackView) this.rootView.findViewById(R.id.feedback2)).setCustomAnswer("Definitely Yes", "Yes", "No", "Definitely No");
        ((FeedbackView) this.rootView.findViewById(R.id.feedback3)).setCustomAnswer("Very Good", "Good", "Bad", "Very Bad");
        ((FeedbackView) this.rootView.findViewById(R.id.feedback4)).setCustomAnswer("Yes", "", "", "No");
        this.rootView.findViewById(R.id.done).setOnClickListener(this);
        ((HomeActivity) getActivity()).backTitle(this.backTitle);
        return this.rootView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                submitFeedback();
            default:
        }
    }

    private void submitFeedback() {
        FeedbackView feedback1 = (FeedbackView) this.rootView.findViewById(R.id.feedback1);
        FeedbackView feedback2 = (FeedbackView) this.rootView.findViewById(R.id.feedback2);
        FeedbackView feedback3 = (FeedbackView) this.rootView.findViewById(R.id.feedback3);
        FeedbackView feedback4 = (FeedbackView) this.rootView.findViewById(R.id.feedback4);
        if (feedback1.getSelection() == 0 || feedback2.getSelection() == 0 || feedback3.getSelection() == 0 || feedback4.getSelection() == 0) {
            new Builder(getActivity()).setTitle("Please answer all questions!").setMessage("Please answer all the questions before submitting feedback! :)").setNeutralButton("Ok", null).show();
            return;
        }
        ((HomeActivity) getActivity())
                .mail(getString(R.string.feedback_email).split(";"),
                getString(R.string.feedback_subject),
                getString(R.string.feedback_body, HomeActivity.championshipname, feedback1.getAnswer(), feedback2.getAnswer(), feedback3.getAnswer(), feedback4.getAnswer()));
    }
}
