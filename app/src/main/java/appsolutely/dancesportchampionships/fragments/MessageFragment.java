package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;

public class MessageFragment extends Fragment implements CacheCallback {
    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_message, container, false);
        ((HomeActivity) getActivity()).backTitle("Audience");
        initializeEvents();

        ImageView imgViewMsg = (ImageView)rootView.findViewById(R.id.message_imageview);
        Picasso.get()
                .load(Urls.ORGANISER_PICTURE)
                .resize(400,600)
                .into(imgViewMsg);
        return this.rootView;
    }

    private void initializeEvents() {
        DataCache.get(Urls.ORGANISER_MESSAGE, this);
    }

    public void onContent(String url, String content) {
        if (this.rootView != null && url.equals(Urls.ORGANISER_MESSAGE) && this.rootView != null) {
            TextView textView = (TextView) this.rootView.findViewById(R.id.message_content_textview);
            textView.setVisibility(View.VISIBLE);
            textView.setText(content);
        }
    }
}
