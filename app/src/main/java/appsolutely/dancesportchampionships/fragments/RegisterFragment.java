package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant;

public class RegisterFragment extends Fragment implements OnClickListener {
    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_registration, container, false);
        this.rootView.findViewById(R.id.done).setOnClickListener(this);
        ImageView imgViewLogo = (ImageView) rootView.findViewById(R.id.imgView);
        Picasso.get()
                .load(Constant.Urls.EVENT_LOGO)
                .into(imgViewLogo);
        ((HomeActivity) getActivity()).backTitle(HomeFragment.getName(HomeFragment.COMPETITORS));
        return this.rootView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done /*2131296267*/:
                checkRegister();
            default:
        }
    }

    private void checkRegister() {
        String nameMale = ((EditText) this.rootView.findViewById(R.id.input_name_m)).getText().toString();
        String nameFemale = ((EditText) this.rootView.findViewById(R.id.input_name_f)).getText().toString();
        String schoolMale = ((EditText) this.rootView.findViewById(R.id.input_school_m)).getText().toString();
        String schoolFemale = ((EditText) this.rootView.findViewById(R.id.input_school_f)).getText().toString();
        String email = ((EditText) this.rootView.findViewById(R.id.input_email)).getText().toString();
        String phone = ((EditText) this.rootView.findViewById(R.id.input_phone)).getText().toString();
        String events = ((EditText) this.rootView.findViewById(R.id.input_events)).getText().toString();
        String content = getString(R.string.register_mailbody, new Object[]{nameMale, nameFemale, schoolMale, schoolFemale, email, phone, events, HomeActivity.championshipname});
        ((HomeActivity) getActivity()).mail(getString(R.string.register_email_receipient).split(";"), getString(R.string.register_mailtitle, new Object[]{HomeActivity.championshipname}), content);
    }
}
