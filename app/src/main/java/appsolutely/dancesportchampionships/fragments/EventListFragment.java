package appsolutely.dancesportchampionships.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.CsvContent;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;
import appsolutely.dancesportchampionships.utilities.EventGroup;
import appsolutely.dancesportchampionships.utilities.EventItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventListFragment extends Fragment implements CacheCallback, OnClickListener {
    private GalleryAdapter adapter;
    private OnSelectEvent listener;
    private View rootView;

    class GalleryAdapter extends ArrayAdapter<EventGroup> {
        private OnSelectEvent listener;

        public GalleryAdapter(Context context, int resource, List<EventGroup> objects, OnSelectEvent listener) {
            super(context, resource, objects);
            this.listener = listener;
        }

        public int getCount() {
            return super.getCount();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_eventlist, (LinearLayout) convertView, false);
            }
            final EventGroup event = (EventGroup) getItem(position);
            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.event(event);
                    }
                }
            });
            ((TextView) convertView.findViewById(R.id.text)).setText(event.name);
            return convertView;
        }
    }

    public interface OnSelectEvent {
        void event(EventGroup eventGroup);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void setListener(OnSelectEvent listener) {
        this.listener = listener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_eventlist, container, false);
        ((HomeActivity) getActivity()).backTitle("Audience");
        this.adapter = new GalleryAdapter(getActivity(), 0, new ArrayList(), this.listener);
        initializeGallery();
        return this.rootView;
    }

    private void initializeGallery() {
        getActivity().getWindowManager().getDefaultDisplay().getSize(new Point());
        ((ListView) this.rootView.findViewById(R.id.listview)).setAdapter(this.adapter);
        DataCache.get(Urls.EVENTS_LIST, this);
    }

    public void onClick(View v) {
    }

    public void onContent(String url, String result) {
        if (this.rootView != null) {
            CsvContent content = new CsvContent(result);
            this.adapter.clear();
            List<String[]> eventsContent = content.getData();
            Map<String, EventGroup> eventGroups = new HashMap();
            System.out.println(result);
            for (int i = 1; i < eventsContent.size(); i++) {
                String[] data = (String[]) eventsContent.get(i);
                System.out.println(new StringBuilder(String.valueOf(i)).append(":").append(data.length).toString());
                if (data.length == 4) {
                    EventItem item = new EventItem(data[1], data[2], data[3]);
                    EventGroup group = (EventGroup) eventGroups.get(data[0]);
                    if (group == null) {
                        group = new EventGroup(data[0]);
                        this.adapter.add(group);
                    }
                    group.events.add(item);
                }
            }
            adapter.notifyDataSetChanged();
            rootView.findViewById(R.id.progress).setVisibility(View.GONE);
        }
    }
}
