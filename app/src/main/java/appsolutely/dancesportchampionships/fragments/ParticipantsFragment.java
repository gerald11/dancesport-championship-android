package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Participant;
import java.util.List;

public class ParticipantsFragment extends Fragment implements OnItemClickListener {
    private List<Participant> participants;
    private View rootView;

    class ParticipantsAdapter extends ArrayAdapter<Participant> {
        public ParticipantsAdapter(Context context, int resource, List<Participant> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_participant, parent, false);
            }
            ((TextView) convertView).setText((getItem(position)).toString());
            return convertView;
        }
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_participants, container, false);
        ((HomeActivity) getActivity()).backTitle("Confirmation");
        initializeList();
        return this.rootView;
    }

    private void initializeList() {
        ListView listView = (ListView) this.rootView.findViewById(R.id.list);
        listView.setAdapter(new ParticipantsAdapter(getActivity(), 0, this.participants));
        listView.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        ((HomeActivity) getActivity()).confirmationLetter(this.participants.get(position));
    }
}
