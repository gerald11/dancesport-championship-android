package appsolutely.dancesportchampionships.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.HomeActivity.HomeFragment;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.CsvContent;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;
import appsolutely.dancesportchampionships.utilities.Participant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfirmationFragment extends Fragment implements OnClickListener, CacheCallback {
    private List<Participant> participants;
    private View rootView;

    public ConfirmationFragment() {
        this.participants = new ArrayList();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_confirmation, container, false);
        this.rootView.findViewById(R.id.done).setOnClickListener(this);
        ((EditText) this.rootView.findViewById(R.id.input_pin)).setText(((HomeActivity) getActivity()).getPref().getString("pin_code", ""));
        ((HomeActivity) getActivity()).backTitle(HomeFragment.getName(HomeFragment.COMPETITORS));
        loadParticipants();
        return this.rootView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done /*2131296267*/:
                checkPin();
            default:
        }
    }

    public void onContent(String url, String result) {
        if (this.rootView != null) {
            CsvContent content = new CsvContent(result);
            this.participants.clear();
            Map<String, Participant> map = new HashMap();
            List<String[]> confirmationContent = content.getData();
            for (int i = 0; i < confirmationContent.size(); i++) {
                String[] data = (String[]) confirmationContent.get(i);
                if (map.containsKey(data[1])) {
                    ((Participant) map.get(data[1])).events.add(data[4]);
                } else {
                    map.put(data[1], Participant.m8n(data));
                }
            }
            this.participants.addAll(map.values());
            if (this.rootView != null && this.rootView.findViewById(R.id.confirmation_overlay) != null) {
                this.rootView.findViewById(R.id.confirmation_overlay).setVisibility(View.GONE);
            }
        }
    }

    private void loadParticipants() {
        DataCache.get(Urls.CONFIRMATION_LISTING, this);
    }

    private void checkPin() {
        String pin = ((EditText) this.rootView.findViewById(R.id.input_pin)).getText().toString();
        if (pin.length() != 4) {
            Toast.makeText(getActivity(), "Please enter 4 digits", Toast.LENGTH_LONG).show();
            return;
        }
        List<Participant> selection = new ArrayList();
        for (Participant participant : this.participants) {
            if (participant.pin.equals(pin)) {
                selection.add(participant);
            }
        }
        if (selection.isEmpty()) {
            Toast.makeText(getActivity(), "Incorrect PIN", Toast.LENGTH_LONG).show();
            return;
        }
        ((HomeActivity) getActivity()).getPref().edit().putString("pin_code", pin).commit();
        ((HomeActivity) getActivity()).participants(selection);
    }
}
