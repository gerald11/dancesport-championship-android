package appsolutely.dancesportchampionships.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import appsolutely.dancesportchampionships.HomeActivity;
import appsolutely.dancesportchampionships.R;
import appsolutely.dancesportchampionships.utilities.EventGroup;
import appsolutely.dancesportchampionships.utilities.EventItem;
import java.util.List;

public class EventsDetailFragment extends Fragment implements OnClickListener {
    private EventDetailAdapter adapter;
    private EventGroup eventGroup;
    private ListView rootView;

    class EventDetailAdapter extends ArrayAdapter<EventItem> {
        public EventDetailAdapter(Context context, int resource, List<EventItem> objects) {
            super(context, resource, objects);
        }

        public int getCount() {
            return super.getCount();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_eventlistdetail, (LinearLayout) convertView, false);
            }
            EventItem event = (EventItem) getItem(position);
            ((TextView) convertView.findViewById(R.id.text1)).setText(event.tag);
            ((TextView) convertView.findViewById(R.id.text2)).setText(event.name);
            ((TextView) convertView.findViewById(R.id.text3)).setText(event.school);
            return convertView;
        }
    }

    public interface OnSelectEvent {
        void event(EventGroup eventGroup);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void setEventGroup(EventGroup eventGroup) {
        this.eventGroup = eventGroup;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = (ListView) inflater.inflate(R.layout.fragment_eventlistdetail, container, false);
        ((HomeActivity) getActivity()).backTitle(this.eventGroup.name);
        this.adapter = new EventDetailAdapter(getActivity(), 0, this.eventGroup.events);
        initializeGallery();
        return this.rootView;
    }

    private void initializeGallery() {
        getActivity().getWindowManager().getDefaultDisplay().getSize(new Point());
        this.rootView.setAdapter(this.adapter);
    }

    public void onClick(View v) {
    }
}
