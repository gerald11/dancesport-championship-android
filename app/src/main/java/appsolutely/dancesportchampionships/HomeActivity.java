package appsolutely.dancesportchampionships;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import appsolutely.dancesportchampionships.fragments.AdjudicatorsConfirmationFragment;
import appsolutely.dancesportchampionships.fragments.AdjudicatorsFragment;
import appsolutely.dancesportchampionships.fragments.AdjudicatorsFragment.OnSelectJudge;
import appsolutely.dancesportchampionships.fragments.AudienceFragment;
import appsolutely.dancesportchampionships.fragments.CompetitorsFragment;
import appsolutely.dancesportchampionships.fragments.ConfirmationFragment;
import appsolutely.dancesportchampionships.fragments.ConfirmationLetterFragment;
import appsolutely.dancesportchampionships.fragments.EventListFragment;
import appsolutely.dancesportchampionships.fragments.EventListFragment.OnSelectEvent;
import appsolutely.dancesportchampionships.fragments.EventsDetailFragment;
import appsolutely.dancesportchampionships.fragments.EventsFragment;
import appsolutely.dancesportchampionships.fragments.FeedbackFragment;
import appsolutely.dancesportchampionships.fragments.InfoFragment;
import appsolutely.dancesportchampionships.fragments.MagazineFragment;
import appsolutely.dancesportchampionships.fragments.MessageFragment;
import appsolutely.dancesportchampionships.fragments.ParticipantsFragment;
import appsolutely.dancesportchampionships.fragments.PendingWebViewFragment;
import appsolutely.dancesportchampionships.fragments.RegisterFragment;
import appsolutely.dancesportchampionships.fragments.TextFragment;
import appsolutely.dancesportchampionships.fragments.TicketsFragment;
import appsolutely.dancesportchampionships.fragments.WebViewFragment;
import appsolutely.dancesportchampionships.fragments.WebViewFragment.LinkListener;
import appsolutely.dancesportchampionships.utilities.Constant.Urls;
import appsolutely.dancesportchampionships.utilities.DataCache;
import appsolutely.dancesportchampionships.utilities.DataCache.CacheCallback;
import appsolutely.dancesportchampionships.utilities.DownloadUtil;
import appsolutely.dancesportchampionships.utilities.EventGroup;
import appsolutely.dancesportchampionships.utilities.Judge;
import appsolutely.dancesportchampionships.utilities.Participant;

public class HomeActivity extends FragmentActivity implements OnClickListener, OnItemClickListener, OnSelectJudge, OnSelectEvent, LinkListener {
    public static String championshipname = "Championship";
    private static View[] tabs;
    private String backContext;
    private boolean homeDisplayed;

    public enum HomeFragment {
        COMPETITORS,
        AUDIENCE,
        ADJUCATORS,
        MAGAZINE,
        INFO,
        ADJUDICATOR_CONFIRM;

        public static String getName(HomeFragment fragment) {
            switch (fragment) {
                case COMPETITORS:
                    return "Competitors";
                case AUDIENCE:
                    return "Audience";
                case ADJUCATORS:
                    return "Adjudicators";
                case MAGAZINE:
                    return "Magazine";
                case INFO:
                    return "Info";
                case ADJUDICATOR_CONFIRM:
                    return "Adjudicator";
                default:
                    return null;
            }
        }
    }

    public HomeActivity() {
        this.backContext = "";
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getActionBar().setIcon(R.drawable.home);
        DataCache.get(Urls.CHAMPIONSHIP_NAME, new CacheCallback() {
            @Override
            public void onContent(String url, String content) {
                    HomeActivity.championshipname = content;
            }
        });
        try {
            DownloadUtil.request(DownloadUtil.URL_RULES, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (savedInstanceState == null) {
            String fragmentName = getIntent().getStringExtra("fragment");
            if (fragmentName != null && fragmentName.equalsIgnoreCase(HomeFragment.getName(AudienceFragment.fragmentId))) {
                getFragmentManager().beginTransaction().replace(R.id.container, new AudienceFragment(), fragmentName).commit();
            } else if (fragmentName == null || !fragmentName.equalsIgnoreCase(HomeFragment.getName(AdjudicatorsFragment.fragmentId))) {
                getFragmentManager().beginTransaction().replace(R.id.container, new CompetitorsFragment(), fragmentName).commit();
            } else {
                AdjudicatorsFragment fragment = new AdjudicatorsFragment();
                fragment.setListener(this);
                getFragmentManager().beginTransaction().replace(R.id.container, fragment, fragmentName).commit();
            }
            initializeTabs();
        }
    }

    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_competitors /*2131296329*/:
                view(HomeFragment.COMPETITORS);
                break;
            case R.id.tab_audience /*2131296331*/:
                view(HomeFragment.AUDIENCE);
                break;
            case R.id.tab_adjucators /*2131296333*/:
                view(HomeFragment.ADJUCATORS);
                break;
            case R.id.tab_magazine /*2131296333*/:
                view(HomeFragment.MAGAZINE);
                break;
            case R.id.tab_info /*2131296334*/:
                view(HomeFragment.INFO);
                break;
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                if (this.homeDisplayed) {
                    finish();
                } else {
                    onBackPressed();
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String item = (String) parent.getAdapter().getItem(position);
        if (item.equalsIgnoreCase("Live Programme")) {
            pendingWebview(Urls.DANCESCORE_URLS, 1, this.backContext);
        } else if (item.equalsIgnoreCase("Live Recall")) {
            pendingWebview(Urls.DANCESCORE_URLS, 2, this.backContext);
        } else if (item.equalsIgnoreCase("Results")) {
            pendingWebview(Urls.DANCESCORE_URLS, 3, this.backContext);
        } else if (item.equalsIgnoreCase("Competitor List")) {
            pendingWebview(Urls.DANCESCORE_URLS, 0, this.backContext);
        } else if (item.equalsIgnoreCase("Events")) {
            getFragmentManager().beginTransaction().replace(R.id.container, new EventsFragment(), "events").addToBackStack(null).commit();
        } else if (item.equalsIgnoreCase("Rules & Regulations")) {
            textFragment("Rules & Regulations", DownloadUtil.URL_RULES, this.backContext);
        } else if (item.equalsIgnoreCase("Confirmation")) {
            getFragmentManager().beginTransaction().replace(R.id.container, new ConfirmationFragment(), "confirmation").addToBackStack(null).commit();
        } else if (item.equalsIgnoreCase("Feedback")) {
            FeedbackFragment fragment = new FeedbackFragment();
            fragment.setTitle(backContext);
            getFragmentManager().beginTransaction().replace(R.id.container, fragment, "feedback").addToBackStack(null).commit();
        } else if (item.equalsIgnoreCase("Registration")) {
            getFragmentManager().beginTransaction().replace(R.id.container, new RegisterFragment(), "registration").addToBackStack(null).commit();
        } else if (item.equalsIgnoreCase("Tickets")) {
            getFragmentManager().beginTransaction().replace(R.id.container, new TicketsFragment(), "tickets").addToBackStack(null).commit();
        } else if (item.equalsIgnoreCase("Organiser's Message")) {
            getFragmentManager().beginTransaction().replace(R.id.container, new MessageFragment(), "message").addToBackStack(null).commit();
        } else if (item.equalsIgnoreCase("Events List")) {
            EventListFragment fragment = new EventListFragment();
            fragment.setListener(this);
            getFragmentManager().beginTransaction().replace(R.id.container, fragment , "eventslist").addToBackStack(null).commit();
        } else {
            Toast.makeText(getApplicationContext(), R.string.feature_not_ready, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint({"InflateParams"})
    public void beaueTitle(String title, boolean home) {
        this.homeDisplayed = home;
        getActionBar().setDisplayShowHomeEnabled(home);
        getActionBar().setDisplayShowCustomEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(false);
        getActionBar().setHomeButtonEnabled(home);
        TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.actionbar_title, null);
        tv.setText(title);
        getActionBar().setCustomView(tv);
        tv.setLayoutParams(new LayoutParams(-1, -1));
    }

    public void setBackContext(String title) {
        this.backContext = title;
    }

    public void backTitle(String title) {
        this.homeDisplayed = false;
        getActionBar().setTitle(title);
        getActionBar().setDisplayShowCustomEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void judge(Judge judge) {
        hideKeyboard();
        webview("https://docs.google.com/viewer?url=" + judge.pdfUrl + "?dl=1", this.backContext);
    }

    public void event(EventGroup group) {
        EventsDetailFragment fragment = new EventsDetailFragment();
        fragment.setEventGroup(group);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, "eventsdetail").addToBackStack(null).commit();
    }

    public void participants(List<Participant> participants) {
        hideKeyboard();
        ParticipantsFragment fragment = new ParticipantsFragment();
        fragment.setParticipants(participants);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, "participants").addToBackStack(null).commit();
    }

    public void confirmationLetter(Participant participant) {
        ConfirmationLetterFragment fragment = new ConfirmationLetterFragment();
        fragment.setParticipant(participant);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, "confirmationletter").addToBackStack(null).commit();
    }

    private void initializeTabs() {
        int i = 0;
        tabs = new View[]{findViewById(R.id.tab_competitors), findViewById(R.id.tab_audience), findViewById(R.id.tab_adjucators), findViewById(R.id.tab_magazine), findViewById(R.id.tab_info)};
        int length = tabs.length;
        while (i < length) {
            tabs[i].setOnClickListener(this);
            i++;
        }
    }

    public void highlightTab(HomeFragment fragment) {
        for (View tab : tabs) {
            if (tab.isActivated()) {
                tab.setActivated(false);
                break;
            }
        }
        View view = null;
        switch (fragment) {
            case COMPETITORS:
                view = findViewById(R.id.tab_competitors);
                break;
            case AUDIENCE:
                view = findViewById(R.id.tab_audience);
                break;
            case ADJUCATORS:
            case ADJUDICATOR_CONFIRM:
                view = findViewById(R.id.tab_adjucators);
                break;
            case MAGAZINE:
                view = findViewById(R.id.tab_magazine);
                break;
            case INFO:
                view = findViewById(R.id.tab_info);
                break;
        }
        if (view != null) {
            view.setActivated(true);
        }
    }

    public void judge(int position) {
        AdjudicatorsConfirmationFragment fragment = new AdjudicatorsConfirmationFragment();
        fragment.setIndex(position);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, AdjudicatorsConfirmationFragment.fragmentId.name()).addToBackStack(null).commit();
    }

    private void view(HomeFragment fragmentType) {
        hideKeyboard();
        String fragmentName = HomeFragment.getName(fragmentType);
        Fragment fragment = getFragmentManager().findFragmentByTag(fragmentName);
        if (fragment == null) {
            switch (fragmentType) {
                case COMPETITORS:
                    fragment = new CompetitorsFragment();
                    break;
                case AUDIENCE:
                    fragment = new AudienceFragment();
                    break;
                case ADJUCATORS:
                    fragment = new AdjudicatorsFragment();
                    ((AdjudicatorsFragment) fragment).setListener(this);
                    break;
                case MAGAZINE:
                    fragment = new MagazineFragment();
                    break;
                case INFO:
                    fragment = new InfoFragment();
                    ((InfoFragment) fragment).setListener(this);
                    break;
            }
        }
        getFragmentManager().popBackStack(null, 1);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, fragmentName).commit();
    }

    public void mail(String[] recipient, String subject, String content) {
        hideKeyboard();
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.EMAIL", recipient);
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.putExtra("android.intent.extra.TEXT", content);
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 2);
        }
    }

    private void textFragment(String title, String bodyUrl, String backTitle) {
        TextFragment fragment = TextFragment.instance(title, backTitle);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, "textFragment").addToBackStack(null).commit();
        try {
            DownloadUtil.request(bodyUrl, fragment);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void link(String url, String backTitle) {
        webview(url, backTitle);
    }

    private void webview(String url, String backTitle) {
        getFragmentManager().beginTransaction().replace(R.id.container, WebViewFragment.instance(url, backTitle), "webview").addToBackStack(null).commit();
    }

    private void pendingWebview(String url, int index, String backTitle) {
        getFragmentManager().beginTransaction().replace(R.id.container, PendingWebViewFragment.instance(url, index, backTitle), "webview").addToBackStack(null).commit();
    }

    public SharedPreferences getPref() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    public String readAssets(String name) {
        char[] buffer = new char[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
        StringBuilder out = new StringBuilder();
        try {
            Reader in = new InputStreamReader(getAssets().open(name), "UTF-8");
            while (true) {
                int rsz = in.read(buffer, 0, buffer.length);
                if (rsz < 0) {
                    break;
                }
                out.append(buffer, 0, rsz);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return out.toString();
    }
}
